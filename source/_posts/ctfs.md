---
title: CTF Schedule
date: 2020-06-14 23:45:01
tags: CTF
comments: false
---
Here's a quick schedule on all the CTFs that UwUtime will attend, plus a list of points from already completed CTFs!
(All times are in EST/EDT 24H)

* [Zg3r0 CTF](https://ctftime.org/event/1062) | June 15, 2020 11:30 - June 17, 2020 11:30
* [UUTCTF 2020](https://ctftime.org/event/1058) | June 20, 2020 2:30 - June 22, 2020 2:30 
