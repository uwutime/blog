---
title: Ice
date: 2020-06-15 15:05:17
tags: [CTF, TryHackMe]
---
Wonder if they stream "Ice Ice Baby" 🤔

Ice is a TryHackMe room with a "poorly secured" Windows based media server. This room is intended to be a starting room and is used in the Metasploit Red Primer room as an example.
Please note that TryHackMe VMs take a while to start, wait a couple of minutes before doing anything.

You can find the room [here](https://tryhackme.com/room/ice).

Ice is split into 7 tasks (of which you can quickly navigate to using the ToC menu above):
* Connect - Connect to the TryHackMe network!
* Recon - Scan and enumerate our victim!
* Gain Access - Exploit the target vulnerable service to gain a foothold!
* Escalate - Enumerate the machine and find potential privilege escalation paths to gain Admin powers!
* Looting - Learn how to gather additional credentials and crack the saved hashes on the machine.
* Post-Exploitation - Explore post-exploitation actions we can take on Windows.
* Extra Credit - Explore manual exploitation via exploit code found on exploit-db. 

Let's get into it!

## Connect
This step is just for assistance in connecting to the TryHackMe network, you can find details [here](https://tryhackme.com/room/ice).
You can just click "Submit" here and skip to Recon.

## Recon
You'll first want to do a port scan of the machine using nmap, if you want to learn more about nmap, you can use [this](https://tryhackme.com/room/rpnmap).
In this case, we can just use a simple -sV can here, thus the command is as follows: (If you're not using Metasploit for this, remove db_)
`db_nmap -sV XX.XX.XXX.XXX`

Once the scan is complete, you'll see a good list of open ports.
Be warned however, for question 3, "MSRDP" may show up as "ms-wbt-server".

The hostname of the machine can be found on the "Service Info" line, right next as the entry "Host".

## Gain Access
Now that we know the services that are available, let's check for vulnerabilities with the Icecast 2 server running.
You can use [CVE Details](https://www.cvedetails.com/) to look for any public CVEs related to Icecast, do that now.
Hmmm, CVE-2004-1561 seems interesting, it even has a Metasploit module available.

At this point, if you weren't already using the Metasploit console, you'll want to open it now.

Using the Metasploit `search` command, we can find that there's an exploit called `exploit/windows/http/icecast_header`, let's switch to that by doing `use exploit/windows/http/icecast_header` now.
Using `info` to see the module info, we can see that there is only one option empty, the classic `RHOSTS`, let's fill that in with the target's IP using `set RHOSTS XX.XX.XXX.XXX`
At this point you can just enter `run` and you'll be dropped to a meterpreter shell that has access to the target's machine.

## Escalate
Well we're in, now what?

First, let's see what user we're running as, you can use `getuid` to see what the username is.
Next, let's get some more information about the computer, let's run `sysinfo` for system information.

Right, now that we know what we're dealing with, let's look for any exploits, go ahead and run `run post/multi/recon/local_exploit_suggester`, this will take a little bit to run.
Looks like there's 9 exploits detected, good to know.

Let's use the exploit `exploit/windows/local/bypassuac_eventvwr` which we found with the scan.
We'll want to keep the shell, so go ahead and background the shell using `background`.
Alright, time to switch to that exploit, and let's take a look at what options we need to fill out.
Looking at the options, we need to set a session id, go ahead and set that to be the session we put in the background. (You can use `sessions` to see the ID)
LHOST will also need to be set, set that to your IP on the TryHackMe network. (You can use `ip addr show` and it'll most likely be listed under tun0)
We can now run it. (If it doesn't work, try setting `LHOST` again)
If everything went well, we should be dropped to an administrator shell! Just to make sure, run `getprivs` and look for `SeTakeOwnershipPrivilege`.

## Looting
Okay, we have Administrator, let's go migrate to something that'll give us a little more permission.

How about `spoolsv.exe`? Let's migrate to it now using `migrate -N spoolsv.exe`.
Now if we use `getuid`, aha! We have SYSTEM user access!

Time to get some passwords, go ahead and load kiwi using `load kiwi`, open the help menu using `help` so we can figure out what command will let us get credentials.
Looks like it's `creds_all`! Let's run it now.

Oh look at that! It's Dark's password! Although, I'm not going to share it here. ;)

## Post-Exploitation
Before we leave, let's do some interesting stuff.

Quickly open that help menu again, let's try some of the commands in there.

* `hashdump` - This command will dump all of the password hashes on the system
* `screenshare` - This will stream a video of the desktop to your web browser
* `record_mic` - Will record the microphone
* `timestomp` - Mess with the file timestamps
* `golden_ticket_create` - This creates a "golden ticket" for Kerberos, allowing us to login whenever and wherever.

If we want to connect to the machine using RDP, we can enable it using `run post/windows/manage/enable_rdp`.

## Extra Credit
At this point, we've successfully hacked into a Windows 7 machine and got full system access to it!

If you'd like to learn more about this exploit, you can check it out over at the [Exploit Database](https://www.exploit-db.com/exploits/568).

## Closure

That was a cool and interesting look into what a real life attack might be, wasn't it?
While this room didn't go into detail about finding out what the Icecast version is, most of the time it'll be listed on the public web GUI. (Although, that was disabled here)

If you want to learn about getting into systems without Metasploit, look no further than the sequel to this room, [Blaster](https://tryhackme.com/room/blaster). (Of which, a write-up will be created soon)
